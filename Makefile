# SPDX-License-Identifier: NCSA
#
# This file is part of avlouis's building-blocks.
# https://gitlab.com/avlouis/building-blocks
#
# It is licensed under the terms of the University of Illinois/NCSA
# Open Source License.  See the LICENSE file for detailed information
# regarding the copyright and license terms.
#
# Copyright 2021 Andrew V. Louis <avlouis@avlouis.com>

CXX ?= g++

DOC = doc
INC = inc
SRC = src
BLD = build
TEST = test
THIRD_PARTY = third_party
CATCH = $(THIRD_PARTY)/catch

CXX_FLAGS = -I$(INC) -I$(CATCH) -pedantic -std=c++17 -Wall -Wextra

TEST_SOURCES = $(wildcard $(TEST)/*.cpp)
TEST_OBJECTS = $(patsubst %.cpp,$(BLD)/%.o,$(TEST_SOURCES))

ALL_OBJECTS = $(TEST_OBJECTS)

ALL_DEPENDENCIES = $(patsubst %.o,%.d,$(ALL_OBJECTS))

define def_mkdir_target
$(1):
	mkdir -p $(1)
endef

.PHONY: all clean doc

all: $(BLD)/avlouis-bb-test

clean:
	-rm -r $(BLD)
	-rm -r $(DOC)

doc:
	doxygen doxyfile

################################################################################
# AUTOMATIC DEPENDENCIES                                                       #
################################################################################
-include $(ALL_DEPENDENCIES)

################################################################################
# DIRECTORY TARGETS                                                            #
################################################################################
$(foreach d,$(sort $(dir $(ALL_OBJECTS))),$(eval $(call def_mkdir_target,$(d))))

################################################################################
# OBJECT GENERATION                                                            #
################################################################################
.SECONDEXPANSION:

$(BLD)/%.o: %.cpp | $$(@D)/
	$(CXX) $(CXX_FLAGS) -c -g -MMD -MP -o $@ $<

################################################################################
# BINARY GENERATION                                                            #
################################################################################

$(BLD)/avlouis-bb-test: $(TEST_OBJECTS) | $$(@D)/
	$(CXX) -g -o $@ $^
