// SPDX-License-Identifier: NCSA
//
// This file is part of avlouis's building-blocks.
// https://gitlab.com/avlouis/building-blocks
//
// It is licensed under the terms of the University of Illinois/NCSA
// Open Source License.  See the LICENSE file for detailed information
// regarding the copyright and license terms.
//
// Copyright 2021 Andrew V. Louis <avlouis@avlouis.com>

#ifndef AVLOUIS_BB_QUICKSELECT_HPP_INCLUDED
#define AVLOUIS_BB_QUICKSELECT_HPP_INCLUDED

#include <cassert>
#include <functional>
#include <random>
#include <vector>

#include "partition.hpp"

namespace avlouis_bb
{

/**
 * Find the \p k th element of a list.
 *
 * It is asserted that \p left \f$ \leq \f$ \p k \f$ \leq \f$ \p right
 * \f$ < \f$ \p list.size().
 *
 * It is assumed that \p list is partitioned into three segments \f$ A
 * \f$, \f$ B \f$, and \f$ C \f$ such that all elements in \f$ A \f$
 * are less than all elements in \f$ B \f$ and all elements in \f$ B
 * \f$ are less than all elements in \f$ C \f$.
 *
 * More precisely, given the following:
 *
 * \f$ A \f$ is the set of indices \f$ a \f$ such that \f$ a \geq 0
 * \f$ and \f$ a < \f$ \p left
 *
 * \f$ B \f$ is the set of indices \f$ b \f$ such that \p left \f$
 * \leq b \leq \f$ \p right
 *
 * \f$ C \f$ is the set of indices \f$ c \f$ such that \f$ c > \f$ \p
 * right and \f$ c < \f$ \p list.size()
 *
 * it is assumed that \f$ \forall (\texttt{a}, \texttt{b}, \texttt{c})
 * \in A \times B \times C \f$, \p list[a] \f$ < \f$ list[b] \f$ < \f$
 * list[c].  If a set \f$A\f$,\f$B\f$, or \f$C\f$ is empty, then the
 * inequality or inequalities with which it is involved are vacuously
 * true.
 *
 * @tparam T The list element value type.
 * @tparam S The list size and index type.
 * @param list The list of data from which to select.
 * @param left The left-most index of the data.
 * @param right The right-most index of the data.
 * @param k The selected index.
 * @param compare The comparison function.
 * @return The value of the \f$ \texttt{k}^{th} \f$ element.
 */
template <typename T, typename S = typename std::vector<T>::size_type>
T quickselect(
    std::vector<T> & list, S left, S right, S k,
    std::function<bool(const T &, const T &)> compare =
        [](const T & l, const T & r) { return l < r; })
{
    assert(left <= k);
    assert(k <= right);
    assert(right < list.size());

    if(left == right) {
        return list[left];
    }

    std::random_device rd;
    std::default_random_engine rd_eng(rd());
    std::uniform_int_distribution<S> dist(left, right);
    const S pivot = dist(rd_eng);
    const S pivot_dest = partition(list, left, right, pivot, compare);
    if(k == pivot_dest) {
        return list[k];
    } else if(k < pivot_dest) {
        return quickselect(list, left, pivot_dest - 1, k, compare);
    } else { // k > pivot_dest
        return quickselect(list, pivot_dest + 1, right, k, compare);
    }
}

/**
 * Find the \p k th element of a list.
 *
 * It is asserted that \p list is not empty (has a size greater than
 * 0).
 *
 * This is simply a convenience wrapper for the full ::quickselect.
 *
 * @tparam T The list element value type.
 * @tparam S The list size and index type.
 * @param list The list of data from which to select.
 * @param k The selected index.
 * @param compare The comparison function.
 * @return The value of the \f$ \texttt{k}^{th} \f$ element.
 */
template <typename T, typename S = typename std::vector<T>::size_type>
inline T quickselect(
    std::vector<T> & list, S k,
    std::function<bool(const T &, const T &)> compare =
        [](const T & l, const T & r) { return l < r; })
{
    assert(list.size() > 0);

    return quickselect(list, S(0), list.size() - 1, k, compare);
}

} // namespace avlouis_bb

#endif // AVLOUIS_BB_QUICKSELECT_HPP_INCLUDED
