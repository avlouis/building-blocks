// SPDX-License-Identifier: NCSA
//
// This file is part of avlouis's building-blocks.
// https://gitlab.com/avlouis/building-blocks
//
// It is licensed under the terms of the University of Illinois/NCSA
// Open Source License.  See the LICENSE file for detailed information
// regarding the copyright and license terms.
//
// Copyright 2021 Andrew V. Louis <avlouis@avlouis.com>

#ifndef AVLOUIS_BB_PARTITION_HPP_INCLUDED
#define AVLOUIS_BB_PARTITION_HPP_INCLUDED

#include <algorithm>
#include <cassert>
#include <functional>
#include <vector>

namespace avlouis_bb
{

/**
 * Partition a portion of a list about a pivot element.
 *
 * It is asserted that \p left \f$ \leq \f$ \p pivot \f$ \leq \f$ \p
 * right \f$ < \f$ \p list.size().  All partitioning is restricted to
 * all indices \f$ i \f$ such that \f$ i \in [ \f$ \p left \f$ , \f$
 * \p right \f$ ] \f$.
 *
 * For a pivot element with value \f$ p \f$, the list is modified such
 * that all element values \f$ v \f$ with \p compare(\f$ v \f$, \f$ p
 * \f$) = \f$ \texttt{true} \f$ are listed before the pivot value.
 * All element values \f$ v \f$ with \p compare(\f$ v \f$, \f$ p \f$)
 * = \f$ \texttt{false} \f$ are listed after the pivot value.  With
 * default behavior, \p compare is the less-than relation.  This makes
 * partitioning equivalent to a partial sort with all elements less
 * than the pivot value appearing in some arbitrary order, followed by
 * the pivot value, and finally all elements greater than or equal to
 * the pivot value (again in some arbitrary order).
 *
 * @tparam T The list element value type.
 * @tparam S The list size and index type.
 * @param list The list of data to partition.
 * @param left The left-most index to include in partitioning.
 * @param right The right-most index to include in partitioning.
 * @param pivot The index of the element to use as the pivot.
 * @param compare The comparison function.
 * @return The final index of the original pivot element.
 */
template <typename T, typename S = typename std::vector<T>::size_type>
S partition(
    std::vector<T> & list, S left, S right, S pivot,
    std::function<bool(const T &, const T &)> compare =
        [](const T & l, const T & r) { return l < r; })
{
    assert(left <= pivot);
    assert(pivot <= right);
    assert(right < list.size());

    const T pivot_value = list[pivot];
    std::swap(list[pivot], list[right]);
    S pivot_destination = left;
    for(S i = left; i < right; i++) {
        if(compare(list[i], pivot_value)) {
            std::swap(list[pivot_destination], list[i]);
            pivot_destination++;
        }
    }
    std::swap(list[right], list[pivot_destination]);
    return pivot_destination;
}

} // namespace avlouis_bb

#endif // AVLOUIS_BB_PARTITION_HPP_INCLUDED
