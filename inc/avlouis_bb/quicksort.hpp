// SPDX-License-Identifier: NCSA
//
// This file is part of avlouis's building-blocks.
// https://gitlab.com/avlouis/building-blocks
//
// It is licensed under the terms of the University of Illinois/NCSA
// Open Source License.  See the LICENSE file for detailed information
// regarding the copyright and license terms.
//
// Copyright 2021 Andrew V. Louis <avlouis@avlouis.com>

#ifndef AVLOUIS_BB_QUICKSORT_HPP_INCLUDED
#define AVLOUIS_BB_QUICKSORT_HPP_INCLUDED

#include <cassert>
#include <functional>
#include <vector>

#include "quickselect.hpp"

namespace avlouis_bb
{

/**
 * Sort a portion of a list.
 *
 * @tparam T The list element value type.
 * @tparam S The list size and index type.
 * @param list The list of data to sort.
 * @param left The left-most index of the data.
 * @param right The right-most index of the data.
 * @param compare The comparison function.
 */
template <typename T, typename S = typename std::vector<T>::size_type>
void quicksort(
    std::vector<T> & list, S left, S right,
    std::function<bool(const T &, const T &)> compare =
        [](const T & l, const T & r) { return l < r; })
{
    if(left >= right) {
        return;
    }

    const S mid = (left + right) / 2;
    (void)quickselect(list, left, right, mid, compare);
    quicksort(list, left, mid - 1, compare);
    quicksort(list, mid + 1, right, compare);
}

/**
 * Sort an entire list.
 *
 * It is asserted that \p list is not empty (has a size greater than
 * 0).
 *
 * This is simply a convenince wrapper for the full ::quicksort.
 *
 * @tparam T The list element value type.
 * @tparam S The list size and index type.
 * @param list The list of data to sort.
 * @param compare The comparison function.
 */
template <typename T, typename S = typename std::vector<T>::size_type>
inline void quicksort(
    std::vector<T> & list, std::function<bool(const T &, const T &)> compare =
                               [](const T & l, const T & r) { return l < r; })
{
    assert(list.size() > 0);

    quicksort(list, S(0), list.size() - 1, compare);
}

} // namespace avlouis_bb

#endif // AVLOUIS_BB_QUICKSORT_HPP_INCLUDED
