// SPDX-License-Identifier: NCSA
//
// This file is part of avlouis's building-blocks.
// https://gitlab.com/avlouis/building-blocks
//
// It is licensed under the terms of the University of Illinois/NCSA
// Open Source License.  See the LICENSE file for detailed information
// regarding the copyright and license terms.
//
// Copyright 2021 Andrew V. Louis <avlouis@avlouis.com>

#ifndef AVLOUIS_BB_EMPTY_HPP_INCLUDED
#define AVLOUIS_BB_EMPTY_HPP_INCLUDED

#include <ostream>

namespace avlouis_bb
{

/**
 * An empty type.
 */
struct empty final
{
};

std::ostream & operator<<(std::ostream & os, const empty &)
{
    os << "avlouis_bb::empty";
    return os;
}

} // namespace avlouis_bb

#endif // AVLOUIS_BB_EMPTY_HPP_INCLUDED
