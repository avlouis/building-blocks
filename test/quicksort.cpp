// SPDX-License-Identifier: NCSA
//
// This file is part of avlouis's building-blocks.
// https://gitlab.com/avlouis/building-blocks
//
// It is licensed under the terms of the University of Illinois/NCSA
// Open Source License.  See the LICENSE file for detailed information
// regarding the copyright and license terms.
//
// Copyright 2021 Andrew V. Louis <avlouis@avlouis.com>

#include <avlouis_bb/quicksort.hpp>
#include <catch.hpp>

using namespace avlouis_bb;

TEST_CASE("quicksort is tested", "[avlouis_bb::quicksort]")
{
    using S = typename std::vector<int>::size_type;
    std::vector<int> test_data = {27, 46, 66, 39, -98, 70, -88, 99, 45, -63};
    const std::vector<int> sorted_data = {-98, -88, -63, 27, 39,
                                          45,  46,  66,  70, 99};
    quicksort(test_data);
    for(S i = 0; i < test_data.size(); i++) {
        REQUIRE(test_data[i] == sorted_data[i]);
    }
}
