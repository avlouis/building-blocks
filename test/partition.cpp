// SPDX-License-Identifier: NCSA
//
// This file is part of avlouis's building-blocks.
// https://gitlab.com/avlouis/building-blocks
//
// It is licensed under the terms of the University of Illinois/NCSA
// Open Source License.  See the LICENSE file for detailed information
// regarding the copyright and license terms.
//
// Copyright 2021 Andrew V. Louis <avlouis@avlouis.com>

#include <avlouis_bb/partition.hpp>
#include <catch.hpp>

using namespace avlouis_bb;

TEST_CASE("avlouis_bb::partition is tested", "[avlouis_bb::partition]")
{
    SECTION("[27,46,66,39,-98,70,-88,99,45,-63]")
    {
        using S = typename std::vector<int>::size_type;
        const std::vector<int> test_data = {27, 46,  66, 39, -98,
                                            70, -88, 99, 45, -63};
        const std::vector<int> sorted_data = {-98, -88, -63, 27, 39,
                                              45,  46,  66,  70, 99};
        SECTION("direct")
        {
            for(S i = 0; i < test_data.size(); i++) {
                DYNAMIC_SECTION("Partition about element " << i)
                {
                    std::vector<int> data = test_data;
                    S pivot_index = partition(data, S(0), data.size() - 1, i);
                    REQUIRE(sorted_data[pivot_index] == data[pivot_index]);
                    for(S i = 0; i < pivot_index; i++) {
                        REQUIRE(data[i] < data[pivot_index]);
                    }
                    for(S i = pivot_index + 1; i < data.size(); i++) {
                        REQUIRE(data[i] > data[pivot_index]);
                    }
                }
            }
        }
        SECTION("indirect")
        {
            for(S i = 0; i < test_data.size(); i++) {
                DYNAMIC_SECTION("Partition about element " << i)
                {
                    std::vector<S> data(test_data.size());
                    std::iota(data.begin(), data.end(), 0);
                    const std::function<bool(const size_t &, const size_t &)>
                        comparator = [=](const size_t & l, const size_t & r) {
                            return test_data[l] < test_data[r];
                        };
                    S pivot_index =
                        partition(data, S(0), data.size() - 1, i, comparator);
                    REQUIRE(sorted_data[pivot_index] ==
                            test_data[data[pivot_index]]);
                    for(S i = 0; i < pivot_index; i++) {
                        REQUIRE(test_data[data[i]] <
                                test_data[data[pivot_index]]);
                    }
                    for(S i = pivot_index + 1; i < test_data.size(); i++) {
                        REQUIRE(test_data[data[i]] >
                                test_data[data[pivot_index]]);
                    }
                }
            }
        }
    }
}
