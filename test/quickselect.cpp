// SPDX-License-Identifier: NCSA
//
// This file is part of avlouis's building-blocks.
// https://gitlab.com/avlouis/building-blocks
//
// It is licensed under the terms of the University of Illinois/NCSA
// Open Source License.  See the LICENSE file for detailed information
// regarding the copyright and license terms.
//
// Copyright 2021 Andrew V. Louis <avlouis@avlouis.com>

#include <avlouis_bb/quickselect.hpp>
#include <catch.hpp>

using namespace avlouis_bb;

TEST_CASE("quickselect is tested", "[avlouis_bb::quickselect]")
{
    SECTION("[27,46,66,39,-98,70,-88,99,45,-63]")
    {
        using S = typename std::vector<int>::size_type;
        const std::vector<int> test_data = {27, 46,  66, 39, -98,
                                            70, -88, 99, 45, -63};
        const std::vector<int> sorted_data = {-98, -88, -63, 27, 39,
                                              45,  46,  66,  70, 99};
        for(S i = 0; i < test_data.size(); i++) {
            DYNAMIC_SECTION("Quickselect element " << i)
            {
                std::vector<int> data = test_data;
                const int val = quickselect(data, i);
                REQUIRE(val == sorted_data[i]);

                for(S j = 0; j < test_data.size(); j++) {
                    if(j < i) {
                        REQUIRE(data[j] < val);
                    } else if(j > i) {
                        REQUIRE(data[j] >= val);
                    } else {
                        REQUIRE(data[j] == val);
                    }
                }
            }
        }
    }

    SECTION("[17,25,10,27,18,11,16,27,29,0]")
    {
        using S = typename std::vector<size_t>::size_type;
        const std::vector<size_t> test_data = {17, 25, 10, 27, 18,
                                               11, 16, 27, 29, 0};
        const std::vector<size_t> sorted_data = {0,  10, 11, 16, 17,
                                                 18, 25, 27, 27, 29};
        SECTION("direct")
        {
            for(S i = 0; i < test_data.size(); i++) {
                DYNAMIC_SECTION("Quickselect element " << i)
                {
                    std::vector<size_t> data = test_data;
                    const size_t val = quickselect(data, i);
                    REQUIRE(val == sorted_data[i]);

                    for(S j = 0; j < test_data.size(); j++) {
                        if(j < i) {
                            REQUIRE(data[j] <= val);
                        } else if(j > i) {
                            REQUIRE(data[j] >= val);
                        } else {
                            REQUIRE(data[j] == val);
                        }
                    }
                }
            }
        }
        SECTION("indirect")
        {
            for(S i = 0; i < test_data.size(); i++) {
                DYNAMIC_SECTION("Quickselect element " << i)
                {
                    std::vector<size_t> data(test_data.size());
                    std::iota(data.begin(), data.end(), 0);
                    const std::function<bool(const size_t &, const size_t &)>
                        comparator = [=](const size_t & l, const size_t & r) {
                            return test_data[l] < test_data[r];
                        };
                    const size_t val = quickselect(data, i, comparator);
                    REQUIRE(test_data[val] == sorted_data[i]);

                    for(S j = 0; j < test_data.size(); j++) {
                        if(j < i) {
                            REQUIRE(test_data[data[j]] <= sorted_data[i]);
                        } else if(j > i) {
                            REQUIRE(test_data[data[j]] >= sorted_data[i]);
                        } else {
                            REQUIRE(test_data[data[j]] == sorted_data[i]);
                        }
                    }
                }
            }
        }
    }

    SECTION("[10,22,19,25,17,24,12,2,27,23]")
    {
        using S = typename std::vector<size_t>::size_type;
        const std::vector<size_t> test_data = {10, 22, 19, 25, 17,
                                               24, 12, 2,  27, 23};
        const std::vector<size_t> sorted_data = {2,  10, 12, 17, 19,
                                                 22, 23, 24, 25, 27};
        SECTION("direct")
        {
            for(S i = 0; i < test_data.size(); i++) {
                DYNAMIC_SECTION("Quickselect element " << i)
                {
                    std::vector<size_t> data = test_data;
                    const size_t val = quickselect(data, i);
                    REQUIRE(val == sorted_data[i]);

                    for(S j = 0; j < test_data.size(); j++) {
                        if(j < i) {
                            REQUIRE(data[j] <= val);
                        } else if(j > i) {
                            REQUIRE(data[j] >= val);
                        } else {
                            REQUIRE(data[j] == val);
                        }
                    }
                }
            }
        }
        SECTION("indirect")
        {
            for(S i = 0; i < test_data.size(); i++) {
                DYNAMIC_SECTION("Quickselect element " << i)
                {
                    std::vector<size_t> data(test_data.size());
                    std::iota(data.begin(), data.end(), 0);
                    const std::function<bool(const size_t &, const size_t &)>
                        comparator = [=](const size_t & l, const size_t & r) {
                            return test_data[l] < test_data[r];
                        };
                    const size_t val = quickselect(data, i, comparator);
                    REQUIRE(test_data[val] == sorted_data[i]);

                    for(S j = 0; j < test_data.size(); j++) {
                        if(j < i) {
                            REQUIRE(test_data[data[j]] <= sorted_data[i]);
                        } else if(j > i) {
                            REQUIRE(test_data[data[j]] >= sorted_data[i]);
                        } else {
                            REQUIRE(test_data[data[j]] == sorted_data[i]);
                        }
                    }
                }
            }
        }
    }
}
