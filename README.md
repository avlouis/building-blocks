# building-blocks

A variety of building blocks of varying utility.

The goal of this library is to provide detailed and specific
implementations in addition to abstract implementations.  In other
words, this library intends to empower developers by giving them the
exact tools they need without hiding the inner workings.
Additionally, the library is licensed with minimal burden placed on
the user.  As such, contributions are greatly appreciated.  If you are
unable to share your changes for any reason, opening an issue is still
a great help.

Development is currently focused on C++, but this is not a restriction
against adding other languages in the future.

## Usage

The library itself is currently header-only.  To incorporate it into
your project, simply direct your compiler to add `inc` as an include
path.

Doxygen documentation can be built by running `make doc` (assuming you
have `doxygen` installed).  Simply direct your browser to
`doc/html/index.html` to view the HTML documentation.

Tests can be built by running `make`.  The test binary is located at
`build/avlouis-bb-test`.

## Contributions

All C++ source code must be styled using `clang-format` with the
included configuration file.

## Third-party Software

Some pieces of third-party software are included to aid development.
These are all placed in the `third_party` directory to maintain
license clarity.  Please consult each project folder for licensing
details as they will differ from the license terms of the
building-blocks project.
